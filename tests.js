/*  You will need nodeunit to run these tests. See:
https://github.com/caolan/nodeunit */

var bootstrap = require('./bootstrap');
var express = require('express');
var url = require('url');
var MongoConnection = require('mongodb').Connection;

module.exports = {
  setUp: function (callback) {
    this.app = express();
    this.settings = require('./settings');
    try {
      this.settingsLocal = require('./settingsLocal');
    } catch(e) {
      this.settingsLocal = undefined;
    }
    bootstrap(this.app, true);
    callback();
  },

  tearDown: function (callback) {
    // clean up
    this.app.get('db').unset();
    callback();
  },

  testInitial: function(test) {
    test.ok(this.app.get('unitTesting'));
    test.done();
  },

  testFindSettings: function (test) {
    test.ok(this.settings.serverPort);
    test.equal(typeof(this.settings.serverPort), 'number');
    for (var prop in this.settings) {
      if (typeof(this.settingsLocal) === 'undefined' || ! this.settingsLocal.hasOwnProperty(prop)) {
        test.equal(this.settings[prop], this.app.get(prop));
      } else {
        test.equal(this.settingsLocal[prop], this.app.get(prop));
      }
    }
    test.done();
  },

  testFindSiteModules: function (test) {
    var siteModules = this.app.get('siteModules');
    test.ok(siteModules instanceof Array);
    test.ok(siteModules.length > 0);
    test.ok(this.app.get('siteModules').indexOf('siteModules'));
    test.done();
  },

  testRouting: function (test) {
    var appGetRoutes = this.app.routes.get.map(function (route) {
      return route.path;
    });
    test.ok(appGetRoutes.indexOf('/test'));
    test.ok(appGetRoutes.indexOf('/test/list'));
    test.ok(appGetRoutes.indexOf('/test/item'));
    test.done();
  },

  testDbSetup: function (test) {
    test.expect(2);

    // We want to test actual db settings here, NOT those when running as a test
    this.app.get('db').unset();
    bootstrap(this.app);

    var parsedDbSource = url.parse(this.app.get('dbSource'));
    var dbname = parsedDbSource.pathname.substr(1);

    this.app.get('db').singleton(function (err, conn) {
      test.ok(conn);
      test.equals(conn.databaseName, 'mydb');

      // Test can only complete once async db tests are complete
      test.done();
    });
  },

  testDbSetupForUnitTests: function (test) {
    test.expect(2);
    var parsedDbSource = url.parse(this.app.get('dbTestSource'));
    var dbname = parsedDbSource.pathname.substr(1);
    this.app.get('db').singleton(function (err, conn) {
      //console.log(conn);
      test.ok(conn);
      test.equals(conn.databaseName, dbname);
      conn.close();

      // Test can only complete once async db tests are complete
      test.done();
    });
  }
};