var fs = require('fs');
var path = require('path');
var url = require('url');
var MongoConnection = require('mongodb').Connection;
var db = require('./db');

function getSettings() {
  var settings, settingsLog;
  try {
    settings = require('./settingsLocal');
    settingsLog = 'Using default + local settings from settingsLocal.js';
    //console.log('Using local settings from settingsLocal.js');
  } catch(e) {
    console.log(e);
    settings = require('./settings');
    settingsLog = 'Using default settings only';
    //console.log('Using default settings only');
  }
  return [settings, settingsLog];
}

function getSiteModules(files) {
  var siteModules = [];
  files.forEach(function (filename) {
    if (fs.statSync(filename).isDirectory()) {
      try {
        var modpath = path.join(__dirname, filename);
        if (fs.existsSync(path.join(modpath, 'route.js'))) {
          siteModules.push(filename);
        }
      } catch(e) {
        console.log(e);
      }
    }
  });
  return siteModules;
}

module.exports = function(app, unitTesting) {
  // unitTesting = typeof(unitTesting) === 'undefined' ? false : unitTesting;
  unitTesting = false;
  app.set('unitTesting', unitTesting);

  var startupLog = [];
  var settingsParts = getSettings();
  var settings = settingsParts[0];
  startupLog.push(settingsParts[1]);

  // Populate settings
  for (var prop in settings) {
    if (!settings.hasOwnProperty(prop)) {
      continue;
    }
    app.set(prop, settings[prop]);
  }

  // Populate siteModules, routes
  var route = require('./route');
  route(app);
  var siteModules = getSiteModules(fs.readdirSync(__dirname));
  app.set('siteModules', siteModules);
  startupLog.push('Found the following site modules:');
  startupLog.push(app.get('siteModules'));
  for (var i = 0; i < siteModules.length; i++) {
    var modpath = path.join(__dirname, siteModules[i]);
    route = require(path.join(modpath, 'route'));
    route(app);
  }

  // Connect to database
  startupLog.push('Database configured to: ' + app.get('dbSource'));
  if (unitTesting) {
    db.setConnectionString(app.get('dbTestSource'));
  } else {
    db.setConnectionString(app.get('dbSource'));
  }
  app.set('db', db);
  app.set('startupLog', startupLog);
};