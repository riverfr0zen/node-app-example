var handle = require('./handlers');

/* 
  Add routes within this function
*/
module.exports = function(app) {
  app.get('/', handle.index);
};