/*
  Base app settings
*/

var settings = {
  serverPort : 12000,
  // Only host and databasename are absolutely required
  dbSource : 'mongodb://user:password@host:port/databasename',
  dbTestSource: 'mongodb://user:password@host:port/databasename_test'
};

module.exports = settings;