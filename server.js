var bootstrap = require('./bootstrap');
var express = require('express');
var app = express();
bootstrap(app);

serverPort = app.get('serverPort');
app.listen(serverPort);
startupLog = app.get('startupLog');
startupLog.map(function (element){
  console.log(element);
});

app.get('db').singleton(function (err, connectionInstance) {
  if (err === null) {
    console.log("Singleton connection established to database '" + connectionInstance.databaseName + "'");
  } else {
    console.log('Error establishing singleton connection to database');
  }

});

console.log("Listening on port " + serverPort);