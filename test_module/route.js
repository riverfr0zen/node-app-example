var handle = require('./handlers');

module.exports = function(app) {
  app.get('/test', handle.list);
  app.get('/test/appAccess', handle.appAccess);
  app.get('/test/dbAccess', handle.dbAccess);
  app.get('/test/addItem', handle.addItem);
  app.get('/test/list', handle.list);
  app.get('/test/item', handle.item);
};