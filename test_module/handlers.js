exports.appAccess = function (request, response) {
    response.send('According to app, serverPort is ' + request.app.get('serverPort'));
};

exports.dbAccess = function (request, response) {
    request.app.get('db').singleton(function (err, conn) {
      if (err !== null) {
        response.json(500, {error: 'Database connection unavailable!'});
      } else {
        response.json(200, {msg: 'According to app, databaseName is ' + conn.databaseName});
      }
    });
};

exports.addItem = function (request, response) {
  var date = new Date();
  var fruits = ['apple', 'orange', 'banana', 'guava', 'kiwi', 'blueberry'];
  var testItem = {
    title : 'Item created ' + date.getMonth() + '/' + date.getDay() + '/' + date.getYear() + ' @ ' + date.getTime(),
    fruit : fruits[Math.floor(Math.random()*5)]
  };
  request.app.get('db').singleton(function (err, db) {
    if (err !== null) {
      response.json(500, {error: 'Database connection unavailable!'});
    } else {
      db.collection('test_items').insert(testItem, function (err, result) {
        if (err !== null) {
          console.log(err);
          response.json(500, {error: 'Database connection unavailable!'});
        }
        console.log(result);
      });
      response.json({msg: 'Added new item', item: testItem});
    }
  });
};

exports.list = function (request, response) {
  var itemsOut = [];

  request.app.get('db').singleton(function (err, db) {
    if (err !== null) {
      response.json(500, {error: 'Database connection unavailable!'});
    } else {
      collection = db.collection('test_items');
      collection.find().toArray(function (err, items) {
        if (err !== null) {
          console.log(err);
          response.json(500, {error: 'Database connection unavailable!'});
        }
        items.forEach(function (item) {
          itemsOut.push(item);
          console.log(item);
        });
        response.json({msg: 'A Node Test List', items: itemsOut});
      });
    }
  });
  //response.send("A Node Test List\n" + itemsOut);
};

exports.item = function (request, response) {
  request.app.get('db').singleton(function (db) {
    response.send('A Node Test Item');
  });
};