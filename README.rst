Installation:
===============
1. Make sure you have nodejs and mongodb installed on your system. You should
   have access to commands such as: node, npm and mongo

2. From your terminal, change to the directory containing this file, and run 
   the following:

npm install


Configuration:
===============
1. Review the default application settings in settings.js

2. To customize settings for this installation, copy settingsLocal.example.js 
   to settingsLocal.js and follow the instructions in that file.

3. Routes can be added to route.js on the top level or in a siteModule (see 
   test_module/route.js as an example)


Running tests:
===============
1. Make sure nodeunit is installed globally (npm install -g nodeunit)

2. From your terminal, run the following:

nodeunit tests.js


Running the server
===============
1. In your terminal run the following command:

node server
