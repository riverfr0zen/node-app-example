var MongoClient = require('mongodb').MongoClient;
var connectionInstance;
var connectionString;

exports.setConnectionString = function (newConnectionString) {
  connectionString = newConnectionString;
};

exports.unset = function () {
  if (connectionInstance) {
    if (connectionInstance.serverConfig._serverState === 'connected') {
      connectionInstance.close();
    }
    connectionInstance = undefined;
  }
};

exports.singleton = function (callback) {
  //console.log('in singleton');
  if (connectionInstance) {
    // @TODO: Find out if this is the best way to check connection status
    if (connectionInstance.serverConfig._serverState !== 'connected') {
      var err = Error('Database server not connected');
      console.log(err);
      callback(err, null);
    } else {
      //console.log('Found existing');
      //console.log(connectionInstance.serverConfig._serverState);
      callback(null, connectionInstance);
    }
  } else {
    var conn_str = connectionString + '?connectTimeoutMS=3000';
    MongoClient.connect(conn_str, function (err, db) {
      if (err !== null) {
        console.log(err);
        callback(err, null);
        return;
      }
      connectionInstance = db;
      callback(null, connectionInstance);
    });
  }
};

