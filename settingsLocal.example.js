// Ignore the following 2 lines
var settings = require('./settings');
module.exports = settings;

/*
  Copy this file to settingsLocal.js,
  Then override items in settings.js below, e.g.

  settings.foo = 'baz';
*/
