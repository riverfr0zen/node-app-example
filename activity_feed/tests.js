/*  You will need nodeunit to run these tests. See:
https://github.com/caolan/nodeunit */

var bootstrap = require('../bootstrap');
var express = require('express');
var MongoConnection = require('mongodb').Connection;

var Follower = require('./objects').Follower;

module.exports = {
  setUp: function (callback) {
    this.app = express();
    bootstrap(this.app, true);
    callback();
  },

  tearDown: function (callback) {
    // clean up
    this.app.get('db').unset();
    callback();
  },

  testFollowerObject: function(test) {
    user_a = new Follower({
      uid: 20,
      followed_uids: new Array(2, 3, 4, 5),
      followed_gids: new Array(1, 2, 3)
    });

    //console.log(user_a);
    test.equal(user_a.uid, 20);
    test.deepEqual(user_a.followed_uids, new Array(2, 3, 4, 5));
    test.deepEqual(user_a.followed_gids, new Array(1, 2, 3));
    test.done();
  }
};