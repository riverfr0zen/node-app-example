var Event = require('./objects').Event;
var Follower = require('./objects').Follower;

exports.createFollower = function (request, response) {
  try {
    follower = new Follower({
      uid: request.body.uid,
      followed_uids: request.body.followed_uids,
      followed_gids: request.body.followed_gids
    });
    response.json(follower);
  } catch (e) {
    console.log(e.message);
    response.json({'error': e.message});
  }
  // request.app.get('db').singleton(function (err, db) {
  //   if (err !== null) {
  //     response.json(500, {error: 'Database connection unavailable!'});
  //   } else {
  //     db.collection('followers').insert(follower, function (err, result) {
  //       if (err !== null) {
  //         console.log(err);
  //         response.json(500, {error: 'Database connection unavailable!'});
  //       }
  //       console.log(result);
  //     });
  //     response.json({msg: 'Added new item', item: testItem});
  //   }
  // });
};