function Follower (properties) {
  this.uid = properties.uid ? properties.uid : undefined;
  this.followed_uids = properties.followed_uids ? properties.followed_uids : undefined;
  this.followed_gids = properties.followed_gids ? properties.followed_gids : undefined;
}
exports.Follower = Follower;


function Event (properties) {

  this.uid = properties.uid ? properties.uid : undefined;
  this.gid = properties.gid ? properties.gid : undefined;

  this.title = properties.title ? properties.title : undefined;
  this.body = properties.body ? properties.body : undefined;

}
exports.Event = Event;


