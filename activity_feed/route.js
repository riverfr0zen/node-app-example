var handle = require('./handlers');

module.exports = function(app) {
  app.get('/feed/follower/create', handle.createFollower);
};